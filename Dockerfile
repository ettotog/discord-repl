FROM mirror.gcr.io/python:3.13-alpine AS builder
COPY . /usr/src/discord-repl
WORKDIR /usr/src/discord-repl
RUN apk add --virtual build-dependencies build-base
RUN python -m pip install pipenv
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install ./
FROM mirror.gcr.io/python:3.13-alpine AS runtime
COPY --from=builder /usr/src/discord-repl/.venv /usr/src/discord-repl/.venv
WORKDIR /root
COPY entrypoint.sh .
ENTRYPOINT ["/root/entrypoint.sh"]
ENV DOCKER_HOST "unix://var/run/docker.sock"
CMD ["discord-repl", "bot"]
LABEL org.opencontainers.image.authors="Ettore Leandro Tognoli <ettoreleandrotognoli@gmail.com>" \
    org.opencontainers.image.url="https://gitlab.com/ettotog/discord-repl" \
    org.opencontainers.image.source="https://gitlab.com/ettotog/discord-repl" \
    org.opencontainers.image.documentation="https://gitlab.com/ettotog/discord-repl" \
    org.opencontainers.image.licenses="Apache-2.0" \
    org.opencontainers.image.title="Discord REPL" \
    org.opencontainers.image.description="Run code from discord with docker containers"
