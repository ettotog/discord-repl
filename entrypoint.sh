#!/bin/ash
set -e

source "/usr/src/discord-repl/.venv/bin/activate"

exec "$@"
