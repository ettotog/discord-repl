from discord import Client, Intents
from dataclasses import dataclass
from discord.message import Message

from discord_repl.bot.api import MessageHandler
from discord_repl.bot.handler import MessageHandlerChain
from discord_repl.command.predicate import Predicate, E

@dataclass()
class NotMySelf(Predicate[Message]):
    client: Client

    def __call__(self, e: Message) -> bool:
        return e.author.id != self.client.user.id


@dataclass()
class StartByMentioningMe(Predicate[Message]):
    client: Client

    def __call__(self, e: Message) -> bool:
        mention = f'<@{self.client.user.id}>'
        return e.content.startswith(mention)


class ReplBot(Client):
    message_handler: MessageHandlerChain

    def __init__(self, *args, **kwargs):
        super().__init__(*args, intents=Intents.all(), **kwargs)
        self.message_handler = MessageHandlerChain(
            handlers=[],
            predicate=NotMySelf(self),
        )

    async def on_message(self, message: Message):
        await self.message_handler(self, message)
