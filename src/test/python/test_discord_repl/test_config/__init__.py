from unittest import TestCase
from test_discord_repl import RESOURCES
from discord_repl.executor import Executor
from discord_repl.config import load, load_executors


expected_config = {
    'executors': [
        {
            'lang': 'python',
            'extensions': ['.py'],
            'image': 'python',
            'aliases': [],
            'command': 'python {}',
            'file': 'script.py',
        }
    ]
}


class ConfigLoadTest(TestCase):

    def test_load_json(self):
        config = load(RESOURCES.get('config.json'))
        self.assertDictEqual(config, expected_config)

    def test_load_yaml(self):
        config = load(RESOURCES.get('config.yaml'))
        self.assertDictEqual(config, expected_config)

    def test_load_yml(self):
        config = load(RESOURCES.get('config.yml'))
        self.assertDictEqual(config, expected_config)

    def test_load_toml(self):
        config = load(RESOURCES.get('config.toml'))
        self.assertDictEqual(config, expected_config)

    def test_load_executors(self):
        executors = load_executors()
        for executor in executors:
            self.assertIsInstance(executor, Executor)
