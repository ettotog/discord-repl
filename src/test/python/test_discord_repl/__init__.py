from discord_repl.resources import Resources
from pathlib import Path
from unittest.mock import MagicMock

RESOURCES = Resources(Path(__file__).parent.parent.parent.joinpath('resources'))


class AsyncMagicMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)