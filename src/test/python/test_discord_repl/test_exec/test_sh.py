from aiounittest import AsyncTestCase
from test_discord_repl import RESOURCES
import docker
from discord_repl.executor.docker_executor import DockerExecutor

class TestExecShell(AsyncTestCase):

    @classmethod
    def setUpClass(cls):
        cls.executor = DockerExecutor(
            'shell',
            {'sh'},
            '.sh',
            'alpine',
            '/bin/sh {}',
            'script.sh',
        )

    async def test_hello_world(self):
        with open(RESOURCES.get('hello_world.sh')) as input_stream:
            code = input_stream.read()
            result = await self.executor.exec(code)
            self.assertEqual(result, 'Hello World!!!\n')
