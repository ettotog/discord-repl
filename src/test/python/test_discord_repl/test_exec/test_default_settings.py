import asyncio
import os
import unittest
from aiounittest import AsyncTestCase
from discord_repl.config import load_provider
from test_discord_repl import RESOURCES, AsyncMagicMock
from discord_repl.executor.docker_executor import ExecutorListener


class TestExecHelloWorlds(AsyncTestCase):

    @classmethod
    def setUpClass(cls):
        cls.provider = load_provider()

    @unittest.skipIf(os.environ.get('CI'), 'We have not enough resources to run this test')
    async def test_hello_world(self):
        sources = RESOURCES.glob('hello_world.*')

        async def test_source(source):
            executor = self.provider.for_file(source)
            with open(source, 'r') as input_stream:
                output = await executor.exec(input_stream.read())
                self.assertEqual(output,'Hello World!!!\n', f'Failed running {source}')

        tasks = [ test_source(source) for source in sources ]
        await asyncio.gather(*tasks)


    @unittest.skipIf(os.environ.get('CI'), 'We have not enough resources to run this test')
    async def test_sqlite(self):
        source = RESOURCES.get('sqlite.sql')
        executor = self.provider.for_file(source)
        with open(source, 'r') as input_stream:
            output = await executor.exec(input_stream.read())
            self.assertEqual(output,'"Hello World!!!"\n', f'Failed running {source}')
            
            
    @unittest.skipIf(os.environ.get('CI'), 'We have not enough resources to run this test')
    async def test_puml(self):
        source = RESOURCES.get('uml.puml')
        listener = ExecutorListener()
        listener.attach_file = AsyncMagicMock()
        executor = self.provider.for_file(source)
        with open(source, 'r') as input_stream:
            output = await executor.exec(input_stream.read(), listener=listener)
            listener.attach_file.assert_called()
            self.assertEqual(output, "")
            
        