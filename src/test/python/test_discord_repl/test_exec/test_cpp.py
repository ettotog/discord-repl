from aiounittest import AsyncTestCase
from test_discord_repl import RESOURCES
import docker
from discord_repl.executor.docker_executor import DockerExecutor


class TestExecCpp(AsyncTestCase):

    @classmethod
    def setUpClass(cls):
        cls.executor = DockerExecutor(
            'c++',
            {'cpp', 'c++'},
            '.cpp',
            'gcc',
            '/bin/bash -c "gcc -x c++ {} -lstdc++ -o /tmp/a.out;/tmp/a.out"',
            'script.cpp',
        )

    async def test_hello_world(self):
        with open(RESOURCES.get('hello_world.cpp')) as input_stream:
            code = input_stream.read()
            result = await self.executor.exec(code)
            self.assertEqual(result, 'Hello World!!!\n')
